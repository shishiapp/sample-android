package com.shishiapp.shishiandroid

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.shishiapp.shishiandroid.util.UIHelper
import com.shishiapp.trycore.model.Triplet
import com.shishiapp.trycore.view.ColorSwatchView


class ProductDetailSwatchRecyclerViewAdapter(var mTriplets: List<Triplet>, private val mListener: (Int, Triplet) -> Unit) : RecyclerView.Adapter<ProductDetailSwatchRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.swatch_view, parent, false)

        return ViewHolder(view)
    }

    var mSelectedIndex = 0
    fun changeSelection(index:Int) {
        mSelectedIndex = index
        notifyDataSetChanged()
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val triplet = mTriplets[position]
        holder.mItem = triplet
        holder.mIndex = position

        val view = holder.mView as ColorSwatchView


        view.setTryOnItems(triplet.first.tryOnItems)


        if (mSelectedIndex == position) {
            UIHelper.setSwatchBorder(view, true)
        } else {
            UIHelper.setSwatchBorder(view, false)
        }



        holder.mView.setOnClickListener {
            mListener(holder.mIndex, holder.mItem!!)
        }
    }

    override fun getItemCount(): Int {
        return mTriplets.size
    }

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {

        var mItem: Triplet? = null
        var mIndex: Int = 0

    }
}

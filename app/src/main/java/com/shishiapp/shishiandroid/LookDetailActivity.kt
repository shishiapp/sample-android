package com.shishiapp.shishiandroid

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import com.shishiapp.trycore.ShiShiSdk
import com.shishiapp.trycore.model.TripletOfLook
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_look_detail.*


class LookDetailActivity : AppCompatActivity() {

    private var mTriplets: List<TripletOfLook?> = ArrayList()
    private var mAdapter: LookProductRecyclerViewAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_look_detail)



        lookDetailProductList.layoutManager = LinearLayoutManager(this)
        lookDetailProductList.isNestedScrollingEnabled = false

        mAdapter = LookProductRecyclerViewAdapter(mTriplets)
        lookDetailProductList.adapter = mAdapter

        if (ShiShiSdk.isInitialized()) {
            loadEverythingAboutLook()
        } else {
            val realm = Realm.getDefaultInstance()
            ShiShiSdk.initialize(this, realm, baseUrl = "https://sg.shishiapp.com", completion = {
                loadEverythingAboutLook()
            })
        }

        lookDetailTryOnButton.setOnClickListener {
            ShiShiSdk.removeAllMakeup()
            var success = true

            for (i in 0 until mTriplets.size) {

                val triplet = mTriplets[i]

                if (triplet != null) {

                    ShiShiSdk.putTripletInCurrentLook(triplet, false)
                    ShiShiSdk.setSelectedTripletOfLook(null)
                } else {
                    success = false
                }
            }

            if (success) {
                finish()
            }
        }
    }


    fun loadEverythingAboutLook() {

        val jsonString = "{\"tryon_groups\":[{\"items\":[472,473,471],\"parameter\":0,\"style\":46,\"category\":2},{\"items\":[472],\"parameter\":50,\"style\":49,\"category\":2},{\"items\":[528],\"parameter\":0,\"style\":18,\"category\":5}]}"

        ShiShiSdk.getTripletsOfLook(jsonString, success = { triplets, more ->

            mTriplets = triplets
            mAdapter!!.mTriplets = triplets
            mAdapter!!.notifyDataSetChanged()

        }, fail = {})

    }

}

package com.shishiapp.shishiandroid

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.text.TextUtils
import com.shishiapp.trycore.ShiShiSdk
import com.shishiapp.trycore.model.*
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_product_detail.*


class ProductDetailActivity : AppCompatActivity() {

    // A Lipstick concept (Single try-on)
//    private val isConcept = true
//    private val contentIds = arrayListOf(110)

    // Multiple Lipstick product items (Single try-on)
//        private val isConcept = false
//        private val contentIds = arrayListOf(3, 5, 12)

    // A Lipstick product item (Single try-on)
//        private val isConcept = false
//        private val contentIds = arrayListOf(12)

    // A Eyeshadow product item (Non Single try-on)
        private val isConcept = false
        private val contentIds = arrayListOf(660)

    // Multiple Eyeshadow product items (Non Single try-on)
//    private val isConcept = false
//    private val contentIds = arrayListOf(275, 276, 277)

    private var mConcept: Concept? = null
    private var mProductItem: ProductItem? = null
    private var mCategory: ProductCategory? = null

    private var mTriplets: List<Triplet> = ArrayList()
    private var mSelectedIndex = 0

    private var mSelectedProductItemId: Int? = null
    private var mSelectedTryOnGroup: TryOnGroupOfLook? = null

    private var mAdapter:ProductDetailSwatchRecyclerViewAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_detail)



        productDetailSwatchList.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)

        mAdapter = ProductDetailSwatchRecyclerViewAdapter(mTriplets, { index, triplet ->

            mSelectedIndex = index

            refreshView(false)
        })

        productDetailSwatchList.adapter = mAdapter



        if (isConcept) {
            ShiShiSdk.getTripletsOfConcept(contentIds[0], success = { triplets, more ->

                if (triplets.size > 0) {

                    mConcept = triplets[0].third
                    productTitleView.text = mConcept!!.name
                    mCategory = ShiShiSdk.getCategory(triplets[0].first.category_id)

                    mTriplets = triplets
                    refreshView(true)
                }

            }, fail = {})


        } else {
            mSelectedProductItemId = contentIds[0]
            if (contentIds.size == 1) {
                ShiShiSdk.getProductItem(mSelectedProductItemId!!, success = { productItem, more ->

                    mProductItem = productItem
                    productTitleView.text = productItem.name


                    ShiShiSdk.getConceptOfProductItem(mSelectedProductItemId!!, success = { concept, more ->
                        mConcept = concept
                        mCategory = ShiShiSdk.getCategory(concept.categories[0] as Int)

                        if (concept.single_tryon) {
                            ShiShiSdk.getTripletsOfConcept(concept, mCategory!!, success = { triplets, more ->
                                this.mTriplets = triplets
                                refreshView(true)
                            }, fail = {})

                        } else {
                            if (mProductItem != null) {

                                mTriplets = ShiShiSdk.loadTripletsOfProductItem(mProductItem!!, concept, mCategory!!)
                                refreshView(true)
                            }
                        }
                    }, fail = {})

                }, fail = {})

            } else {
                // Show multiple product items of a concept

                ShiShiSdk.getTripletsOfProductItems(contentIds, success = { triplets, more ->

                    if (triplets.size > 0) {
                        mConcept = triplets[0].third
                        productTitleView.text = mConcept!!.name

                        mCategory = ShiShiSdk.getCategory(triplets[0].first.category_id)

                        mTriplets = triplets
                        refreshView(true)
                    }

                }, fail = {})

            }
        }

        productDetailTryOnButton.setOnClickListener {
            val selectedTriplet = mTriplets[mSelectedIndex]

            val selectedTripletOfLook = ShiShiSdk.createTripletOfLook(selectedTriplet)

            ShiShiSdk.putTripletInCurrentLook(selectedTripletOfLook, true)
            finish()
        }
    }


    private fun refreshView(reloadSwatches: Boolean) {

        if (mCategory == null || mConcept == null) {
            return
        }

        if (mTriplets.size == 0) {
            return
        }

        if (reloadSwatches) {
            reloadSwatches()
            doSelectItem(true)
        } else {
            doSelectItem(false)
        }

        mProductItem = mTriplets[mSelectedIndex].second


        Picasso.get().load(mProductItem!!.image!!).placeholder(R.mipmap.placeholder).into(productDetailImage)

        var description = ""
        if (!TextUtils.isEmpty(mConcept!!.concept_description)) {
            description += mConcept!!.concept_description + "\n"
        }

        if (!TextUtils.isEmpty(mConcept!!.ingredient_effect)) {
            description += mConcept!!.ingredient_effect + "\n"
        }


        if (!TextUtils.isEmpty(mConcept!!.howto)) {
            description += mConcept!!.howto
        }

        productDetailDescription.text = description
    }

    private fun reloadSwatches() {

        mAdapter!!.mTriplets = mTriplets
        mAdapter!!.notifyDataSetChanged()

        locateAndSelectItem()
    }

    private fun locateAndSelectItem() {

        mSelectedIndex = 0

        if (mConcept!!.single_tryon) {

            if (mSelectedProductItemId != null) {
                for (i in 0 until mTriplets.size) {
                    if (mTriplets[i].second.pk == mSelectedProductItemId) {
                        mSelectedIndex = i
                        break
                    }
                }
            }
        } else {
            if (mSelectedTryOnGroup != null) {
                for (i in 0 until mTriplets.size) {
                    val tryOnGroup = mTriplets[i].first

                    if (tryOnGroup.equals(mSelectedTryOnGroup!!)) {
                    mSelectedIndex = i
                    break
                }
                }
            }
        }
    }


    private fun doSelectItem(scroll: Boolean) {

        if (mTriplets.size == 0) {
            return
        }

        if (mAdapter != null) {

            mAdapter!!.changeSelection(mSelectedIndex)
        }

        if (scroll) {
            productDetailSwatchList!!.scrollToPosition(mSelectedIndex)
        }
    }
}

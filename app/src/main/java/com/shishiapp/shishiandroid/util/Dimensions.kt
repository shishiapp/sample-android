package com.shishiapp.shishiandroid.util

import android.content.Context
import android.content.res.Configuration
import android.content.res.Resources
import android.graphics.Point
import android.view.WindowManager

object Dimensions {

    fun dipToPixelOffset(dip: Float, resources: Resources): Int {
        return (dip * resources.displayMetrics.density).toInt()
    }

    fun dipToPixelSize(dip: Float, resources: Resources): Int {
        val res = Math.round(dip * resources.displayMetrics.density)
        if (res != 0) return res
        if (dip == 0f) return 0
        return if (dip > 0) 1 else -1
    }

    fun getWidthInPixels(context: Context): Int {
        val wm = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val display = wm.defaultDisplay
        val size = Point()
        display.getSize(size)
        return size.x
    }

    fun getHeightInPixels(context: Context): Int {
        val wm = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val display = wm.defaultDisplay
        val size = Point()
        display.getSize(size)
        return size.y
    }

    fun getStatusBarHeight(context: Context): Int {
        val res = context.resources.getIdentifier("status_bar_height", "dimen", "android")
        return if (res == 0) 0 else context.resources.getDimensionPixelSize(res)
    }

    fun isTablet(context: Context): Boolean {
        return context.resources.configuration.screenLayout and Configuration.SCREENLAYOUT_SIZE_MASK >= Configuration.SCREENLAYOUT_SIZE_LARGE
    }
}

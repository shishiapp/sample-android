package com.shishiapp.shishiandroid.util

import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.view.View

object UIHelper {

    fun setSwatchBorder(view: View, hasBorder:Boolean) {
        if (hasBorder) {
            val border = GradientDrawable()
            border.setColor(-0x1)
            border.setStroke(2, Color.BLACK)
            view.setBackground(border)

        } else {

            val border = GradientDrawable()
            border.setColor(-0x1)
            border.setStroke(0, Color.WHITE)
            view.setBackground(border)
        }
    }

}
package com.shishiapp.shishiandroid.util

import java.io.File


object FileHelper {

    fun removeFile(filePath: String): Boolean {
        val file = File(filePath)
        return file.delete()
    }

    // removes every photo in the folder except our current photo
    fun clearFolder(filePath: String) {
        val file = File(filePath)
        val folderPath = file.parent

        val folder = File(folderPath)
        if (folder.isDirectory) {
            val children = folder.list()
            for (i in children.indices) {
                if (!filePath.contains(children[i])) {
                    File(folder, children[i]).delete()
                }
            }
        }
    }


}

package com.shishiapp.shishiandroid

import android.content.Context
import android.content.Intent
import android.media.MediaPlayer
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.WindowManager
import com.shishiapp.shishiandroid.util.FileHelper
import kotlinx.android.synthetic.main.activity_record.*
import me.eugeniomarletti.extras.intent.IntentExtra
import me.eugeniomarletti.extras.intent.base.String

var Intent.movieFilePath by IntentExtra.String()

fun Context.MovieRecordIntent(path: String): Intent {
    return Intent(this, MovieRecordActivity::class.java).apply {
        movieFilePath = path
    }
}


class MovieRecordActivity : AppCompatActivity() {


    private var mMovieFilePath: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_record)

        window.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)
        mMovieFilePath = intent.movieFilePath


        recordMovieView.setVideoPath(mMovieFilePath)
        recordMovieView.setOnPreparedListener {mp: MediaPlayer ->
            mp.setVolume(5f, 5f)
            mp.isLooping = true
            mp.start()

        }



        closeView.setOnClickListener({
            finish()
            overridePendingTransition(0, 0)
        })

        closeView.setOnClickListener({
            finish()
            overridePendingTransition(0, 0)
        })

    }


    override fun onDestroy() {
        super.onDestroy()
        try {
            FileHelper.removeFile(mMovieFilePath!!)
        } catch (removeFileException: Exception) {
            // somehow file name is changed and photo does not exist
        }

    }

}
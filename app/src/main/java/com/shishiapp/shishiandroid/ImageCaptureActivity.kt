package com.shishiapp.shishiandroid

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.WindowManager
import com.shishiapp.shishiandroid.util.FileHelper
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_capture.*
import me.eugeniomarletti.extras.intent.IntentExtra
import me.eugeniomarletti.extras.intent.base.String
import java.io.File

var Intent.imageFilePath by IntentExtra.String()

fun Context.ImageCaptureIntent(path: String): Intent {
    return Intent(this, ImageCaptureActivity::class.java).apply {
        imageFilePath = path
    }
}


class ImageCaptureActivity : AppCompatActivity() {


    private var mImageFilePath: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_capture)

        window.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)


        mImageFilePath = intent.imageFilePath
        Picasso.get().load(Uri.fromFile(File(mImageFilePath))).into(captureImageView!!)

        //removes all of the files except current photo
        FileHelper.clearFolder(mImageFilePath!!)


        closeView.setOnClickListener({
            finish()
            overridePendingTransition(0, 0)
        })

    }


    override fun onDestroy() {
        super.onDestroy()
        try {
            FileHelper.removeFile(mImageFilePath!!)
        } catch (removeFileException: Exception) {
            // somehow file name is changed and photo does not exist
        }

    }

}
package com.shishiapp.shishiandroid

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.MotionEvent
import android.view.View
import android.view.WindowManager
import android.widget.SeekBar
import com.shishiapp.shishiandroid.view.CircularProgressBar
import com.shishiapp.trycore.MakeupCamView
import com.shishiapp.trycore.ShiShiSdk
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_main.*
import java.io.File
import java.util.*


class MainActivity : AppCompatActivity() {

    private val REQUEST_CODE = 1
    private val PERMISSIONS = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO)

    fun hasPermissions(context: Context?, permissions: Array<String>): Boolean {
        if (context != null && permissions != null) {
            for (permission in permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false
                }
            }
        }
        return true
    }

    private var realm = Realm.getDefaultInstance()
    private var mMakeupCamView: MakeupCamView? = null

    private var recording = false
    private var elapsed = 0L
    private var mTimer: Timer? = null

    private var mProgressBar: CircularProgressBar? = null
    private var mMovieFile: File? = null
    private var beautifyEnabled: Boolean = false

    val INTERVAL = 10L
    val TIMEOUT = 5000L

    val mHandler = Handler()
    val mLongPressed = Runnable {
        if (!recording) {
            runOnUiThread {
                try {
                    recordButtonPressed()
                } catch (ex: Exception) {
                    // Log.i("---","Exception in thread");
                }

            }

            recording = true
            val task = object : TimerTask() {
                override fun run() {
                    elapsed += INTERVAL

                    if (elapsed >= TIMEOUT) {
                        this.cancel()

                        try {
                            runOnUiThread {
                                elapsed = 0
                                recording = false
                                mProgressBar!!.setProgress(0f)

                                recordButtonReleased()

                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }

                        return
                    }

                    runOnUiThread {
                        mProgressBar!!.setProgress(100 * elapsed / TIMEOUT.toFloat())
                    }

                }
            }
            mTimer = Timer()
            mTimer!!.scheduleAtFixedRate(task, 0, INTERVAL)
        }
    }

    private fun recordButtonPressed() {

        if (!hasPermissions(this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, REQUEST_CODE)
        } else {
            mMovieFile = File(Environment.getExternalStorageDirectory(), "movie.mp4")

            mMakeupCamView!!.startRecording(mMovieFile!!)

        }
    }

    fun recordButtonReleased() {

        mMakeupCamView!!.finishRecordingWithCompletionHandler {

            if (mMovieFile != null) {
                startActivity(MovieRecordIntent(mMovieFile!!.path).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION))
            }
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)
        window.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)

        makeupContainer.removeAllViews()

        mMakeupCamView = MakeupCamView(this)

        beautifyEnabled = true
        ShiShiSdk.smoothLevel = 3
        ShiShiSdk.morphLevel = 3

        makeupContainer.addView(mMakeupCamView)

        switchCameraView.setOnClickListener {
            prepareToSwitch()

            mMakeupCamView!!.flipCamera(::cameraFlipped)

        }

        productButton.setOnClickListener {
            startActivity(Intent(this, ProductDetailActivity::class.java))
        }

        lookButton.setOnClickListener {
            startActivity(Intent(this, LookDetailActivity::class.java))
        }


        cameraButton.setOnTouchListener(View.OnTouchListener { v, event ->

            when (event.action) {

                MotionEvent.ACTION_DOWN -> {
                    mHandler.postDelayed(mLongPressed, 500)

                    return@OnTouchListener true
                }


                MotionEvent.ACTION_UP -> {

                    mHandler.removeCallbacks(mLongPressed)

                    if (recording) {

                        recording = false
                        elapsed = 0
                        if (mTimer != null) {
                            mTimer!!.cancel()
                        }

                        recordButtonReleased()

                        mProgressBar!!.setProgress(0f)
                    } else {
                        cameraButtonClicked()
                    }

                    return@OnTouchListener false
                }

            }
            false

        })


        beautifyButton.setOnClickListener {

            beautifyEnabled = !beautifyEnabled
            if (beautifyEnabled) {
                ShiShiSdk.smoothLevel = 3
                ShiShiSdk.morphLevel = 3
            } else {
                ShiShiSdk.smoothLevel = 1
                ShiShiSdk.morphLevel = 0
            }

            updateBeautifyButton()
            ShiShiSdk.syncBeautify(mMakeupCamView!!)
        }

        compareButton.setOnClickListener {

            ShiShiSdk.autoCompareEnabled = !ShiShiSdk.autoCompareEnabled

            updateCompareButton()
            ShiShiSdk.syncAutoCompare(mMakeupCamView!!)
        }


        ShiShiSdk.initialize(this, realm, baseUrl = "https://sg.shishiapp.com", completion = null)


        mProgressBar = recordProgressBar as CircularProgressBar



        slider.max = MakeupCamView.MAX_ZOOM_LEVEL

        slider.setOnSeekBarChangeListener(object: SeekBar.OnSeekBarChangeListener{
            override fun onStartTrackingTouch(p0: SeekBar?) {

            }

            override fun onStopTrackingTouch(p0: SeekBar?) {

            }

            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {

                mMakeupCamView!!.setZoomLevel(progress)
            }
        })

    }

    private fun updateCompareButton() {
        if (ShiShiSdk.autoCompareEnabled) {
            compareButton.setColorFilter(Color.WHITE, android.graphics.PorterDuff.Mode.MULTIPLY)
        } else {
            compareButton.setColorFilter(ContextCompat.getColor(this, R.color.muteWhiteIcon), android.graphics.PorterDuff.Mode.MULTIPLY)
        }

    }

    private fun updateBeautifyButton() {
        if (beautifyEnabled) {
            beautifyButton.setColorFilter(Color.WHITE, android.graphics.PorterDuff.Mode.MULTIPLY)
        } else {
            beautifyButton.setColorFilter(ContextCompat.getColor(this, R.color.muteWhiteIcon), android.graphics.PorterDuff.Mode.MULTIPLY)
        }
    }


    private fun cameraButtonClicked() {
        mMakeupCamView!!.captureImage(this, callback = { file ->
            if (file != null) {
                startActivity(ImageCaptureIntent(file.path).addFlags(android.content.Intent.FLAG_ACTIVITY_NO_ANIMATION))
            }
        })
    }


    public override fun onResume() {

        super.onResume()

        mMakeupCamView!!.resume()

        syncFeatures()
    }


    public override fun onPause() {
        super.onPause()

        mMakeupCamView!!.pause()

        cancelRecording()
    }

    private fun cancelRecording() {
        mHandler.removeCallbacks(mLongPressed)
    }


    private fun prepareToSwitch() {
        switchCameraView.isEnabled = false
    }

    private fun cameraFlipped() {
        switchCameraView.isEnabled = true
        slider.progress = mMakeupCamView!!.getZoomLevel()
    }

    private fun syncFeatures() {

        ShiShiSdk.syncBeautify(mMakeupCamView!!)
        ShiShiSdk.syncAutoCompare(mMakeupCamView!!)
        ShiShiSdk.syncAllMakeup(mMakeupCamView!!)
    }
}

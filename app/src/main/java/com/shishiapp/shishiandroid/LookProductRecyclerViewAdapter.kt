package com.shishiapp.shishiandroid

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.shishiapp.trycore.ShiShiSdk
import com.shishiapp.trycore.model.TripletOfLook
import com.shishiapp.trycore.view.ColorSwatchView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.look_product_item.view.*


class LookProductRecyclerViewAdapter(var mTriplets: List<TripletOfLook?>) : RecyclerView.Adapter<LookProductRecyclerViewAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.look_product_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val triplet = mTriplets[position]!!

        val productItem = triplet.second
        val concept = triplet.third
        val tryOnGroup = triplet.first

        holder.triplet = triplet

        holder.mView.brandNameTextView.text = "Brand" // basic use of the SDK, can't get Brand name. Use client API to get Brand info instead.
        holder.mView.conceptNameTextView.text = concept.name


        val category = ShiShiSdk.getCategory(tryOnGroup.category_id)

        holder.mView.categoryNameTextView.text = category!!.name

        val swatchView = holder.mView.swatchView as ColorSwatchView

        swatchView.setTryOnItems(tryOnGroup.tryon_items)

        if (productItem != null) {
            Picasso.get().load(productItem.image).placeholder(R.mipmap.placeholder).into(holder.mView.productItemImageView)
        }

        val style = tryOnGroup.style!!

        Picasso.get().load(style.image).placeholder(R.mipmap.placeholder).into(holder.mView.styleImageView)

    }

    override fun getItemCount(): Int {
        return mTriplets.size
    }

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {

        var triplet: TripletOfLook? = null

    }
}
